public class Main
{
	final int numberOfRequests = 1000;

	public static void main(String[] argv)
	{
		Main main = new Main();
		main.init();
	}

	public void init()
	{
		for (int i = 0; i < numberOfRequests; i++)
		{
			HttpRequestThread request = new HttpRequestThread(3000);
			new Thread(request).start();
		}
	}
}
