import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequestThread implements Runnable
{
	private int port;

	HttpRequestThread(int port)
	{
		this.port = port;
	}

	public void run()
	{
		try
		{
			URL url = new URL("http://localhost:3000/API2");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			byte[] response = new byte[1000];
			InputStream responseStream = con.getInputStream();
			responseStream.read(response);
			System.out.println(new String(response));
			responseStream.close();
			con.disconnect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
