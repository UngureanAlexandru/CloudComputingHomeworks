import com.sun.net.httpserver.HttpExchange;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Random;

import java.io.File;

public class ClientThread implements Runnable
{
	private HttpExchange exchange;
	private String path;

	ClientThread(HttpExchange exchange, String path)
	{
		this.exchange = exchange;
		this.path = path;
	}

	public void run()
	{
		try
		{
			switch (path)
			{
				case "/":
					htmlHandler(exchange);
				break;

				case "/style.css":
					cssHandler(exchange);
				break;

				case "/create":
					createHandler(exchange);
				break;

				case "/delete":
					deleteHandler(exchange);
				break;

				case "/write":
					writeHandler(exchange);
				break;

				case "/read":
					readHandler(exchange);
				break;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void htmlHandler(HttpExchange exchange) // Just to be here, does nothing
	{
		try
		{
			String response = "response";

			exchange.getRequestBody();
			exchange.sendResponseHeaders(200, response.length());
			exchange.getResponseHeaders().add("Content-Type", "text/html");

			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void cssHandler(HttpExchange exchange) // Just to be here, does nothing
	{
		try
		{
			String response = "response";

			exchange.getRequestBody();
			exchange.sendResponseHeaders(200, response.length());
			exchange.getResponseHeaders().add("Content-Type", "text/css");

			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void createHandler(HttpExchange exchange)
	{
		try
		{
			String argument = exchange.getRequestURI().toString().split("/")[2];

			int pastes = 1;

			System.out.println(argument);
			if (isNumber(argument))
			{
				pastes = Integer.parseInt(argument);

				if (pastes < 1)
				{
					pastes = 1;
				}
			}

			final int NAME_SIZE = 30;
			Random randomGenerator = new Random();

			StringBuilder fileName = new StringBuilder();

			StringBuilder response = new StringBuilder();
			response.append("<root>\n");

			for (int times = 0; times < pastes; times++)
			{
				fileName.setLength(0);
				response.append("\t<response>");

				for (int i = 0; i < NAME_SIZE; i++)
				{
					int newChar = randomGenerator.nextInt(75) + 48;

					while (!checkNumber(newChar))
					{
						newChar = randomGenerator.nextInt(75) + 48;
					}

					fileName.append((char)newChar);
				}
				fileName.append(".paste");

				boolean found = false;

				for (int i = 0; i < Main.idS.size(); i++)
				{
					if (Main.idS.get(i).equals(response.toString()))
					{
						found = true;
						break;
					}
				}

				while (found)
				{
					fileName.setLength(0);

					for (int j = 0; j < NAME_SIZE; j++)
					{
						int newChar = randomGenerator.nextInt(75) + 48;

						while (!checkNumber(newChar))
						{
							newChar = randomGenerator.nextInt(75) + 48;
						}

						fileName.append((char)newChar);
					}
					fileName.append(".paste");
				}

				Main.idS.add(fileName.toString());

				System.out.println(fileName);

				response.append(fileName.toString());
				response.append("</response>\n");

				FileOutputStream fileOutputStream = new FileOutputStream(fileName.toString());
				fileOutputStream.write("".getBytes());
				fileOutputStream.close();
			}


			response.append("</root>");

			exchange.getRequestBody();
			exchange.sendResponseHeaders(200, response.length());
			exchange.getResponseHeaders().add("Content-Type", "text/html");

			OutputStream os = exchange.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void deleteHandler(HttpExchange exchange)
	{
		StringBuilder response = new StringBuilder();
		response.append("<root>\n");

		int responseCode = 200;

		try
		{
			String filePath = exchange.getRequestURI().toString().split("/")[2];

			if (filePath.equals("all"))
			{
				Main.idS.removeAll(Main.idS);
				File root = new File(".");
				File[] files = root.listFiles();

				for (int i = 0; i < files.length; i++)
				{
					if (files[i].getName().endsWith(".paste"))
					{
						if(files[i].delete())
						{
							response.append("\t<response>Delete successful!</response>\n");
						}
						else
						{
							response.append("\t<response>Failed to delete file!</response>\n");
						}
					}
				}
			}
			else
			{
				File file = new File(filePath);
				if (file.delete())
				{
					response.append("\t<response>Delete successful!</response>\n");
					Main.idS.remove(filePath);
				}
				else
				{
					response.append("\t<response>Failed to delete file!</response>\n");
					responseCode = 404;
				}
			}

			response.append("</root>");

			exchange.getRequestBody();
			exchange.sendResponseHeaders(responseCode, response.length());
			exchange.getResponseHeaders().add("Content-Type", "text/html");

			OutputStream os = exchange.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void writeHandler(HttpExchange exchange)
	{
		try
		{
			int responseCode = 200;

			StringBuilder response = new StringBuilder();
			response.append("<root>\n");

			int data;
			StringBuilder postDataBuilder = new StringBuilder();
			while((data = exchange.getRequestBody().read()) > 0)
			{
				postDataBuilder.append((char)data);
			}

			String postFileName = postDataBuilder.toString().split("\"")[2].split("-")[0];
			String postNewData = postDataBuilder.toString().split("\"")[4].split("-")[0];

			//System.out.println("|" + postDataBuilder.toString() + "|");
			postFileName = postFileName.replace("\n", "").replace("\r", "");
			postNewData = postNewData.replace("\n", "").replace("\r", "");

			if (postFileName == "" || postNewData == "")
			{
				responseCode = 204;
			}

			System.out.println("|" + postFileName + "|");
			System.out.println("|" + postNewData + "|");

			boolean found = false;
			for (int i = 0; i < Main.idS.size(); i++)
			{
				if (Main.idS.get(i).equals(postFileName))
				{
					FileOutputStream fileOutputStream = new FileOutputStream(postFileName);
					fileOutputStream.write(postNewData.getBytes());
					fileOutputStream.close();
					found = true;
				}
			}

			response.append("\t<response>");
			if (found)
			{
				response.append("Write done successfully!");
			}
			else
			{
				response.append("Write failed!");
				responseCode = 404;
			}
			response.append("</response>\n");
			response.append("</root>");

			exchange.getRequestBody();
			exchange.sendResponseHeaders(responseCode, response.length());
			exchange.getResponseHeaders().add("Content-Type", "text/html");

			OutputStream os = exchange.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void readHandler(HttpExchange exchange)
	{
		try
		{
			int responseCode = 200;

			StringBuilder response = new StringBuilder();
			response.append("<root>\n");

			String filePath = exchange.getRequestURI().toString().split("/")[2];

			if (filePath.equals("all"))
			{

				for (int i = 0; i < Main.idS.size(); i++)
				{
					response.append("\t<response>");

					FileInputStream fileInputStream = new FileInputStream(Main.idS.get(i));

					StringBuilder responseBuilder = new StringBuilder();
					while(fileInputStream.available() > 0)
					{
						int aux = fileInputStream.read();
						responseBuilder.append((char)aux);
					}

					response.append(responseBuilder.toString());
					response.append("</response>\n");
				}
				response.append("</root>");
			}
			else
			{
				boolean found = false;
				for (int i = 0; i < Main.idS.size(); i++)
				{
					if (Main.idS.get(i).equals(filePath))
					{
						found = true;
					}
				}

				response.append("\t<response>");

				if (found)
				{
					FileInputStream fileInputStream = new FileInputStream(filePath);

					StringBuilder dataBuilder = new StringBuilder();

					while(fileInputStream.available() > 0)
					{
						int aux = fileInputStream.read();
						dataBuilder.append((char)aux);
						System.out.println((char)aux);
					}

					response.append(dataBuilder.toString());
				}
				else
				{
					response.append("File not found!");
					responseCode = 404;
				}
				response.append("</response>\n");
				response.append("</root>");
			}

			exchange.getRequestBody();
			exchange.sendResponseHeaders(responseCode, response.length());
			exchange.getResponseHeaders().add("Content-Type", "text/html");

			OutputStream os = exchange.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private boolean checkNumber(int number)
	{
		if ((number >= 48 && number <= 57) || (number >= 65 && number <= 90) || (number >= 97 && number <= 122))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private boolean isNumber(String number)
	{
		int counter = 0;

		for (int i = 0; i < number.length(); i++)
		{
			for (char c = '0'; c <= '9'; c++)
			{
				if (number.charAt(i) == c)
				{
					counter++;
				}
			}
		}

		if (counter == number.length())
		{
			return true;
		}

		return false;
	}
}
