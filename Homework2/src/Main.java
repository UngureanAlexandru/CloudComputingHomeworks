import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.io.File;

public class Main
{
	private HttpServer httpServer = null;
	private final int PORT = 3000;
	public static ArrayList<String> idS;

	public static void main(String[] args)
	{
		Main main = new Main();
		main.init();
	}

	private void init()
	{
		try
		{
			idS = new ArrayList<String>();

			File root = new File(".");
			File[] files = root.listFiles();

			for (int i = 0; i < files.length; i++)
			{
				if (files[i].getName().endsWith(".paste"))
				{
					idS.add(files[i].getName());
					System.out.println(files[i].getName());
				}
			}

			httpServer = HttpServer.create(new InetSocketAddress(PORT), 0);

			httpServer.createContext("/", this::htmlHandler);
			httpServer.createContext("/style.css", this::cssHandler);

			httpServer.createContext("/create", this::createHandler);
			httpServer.createContext("/delete", this::deleteHandler);
			httpServer.createContext("/write", this::writeHandler);
			httpServer.createContext("/read", this::readHandler);

			httpServer.setExecutor(null);
			httpServer.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void htmlHandler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(exchange, "/");
		new Thread(clientThread).start();
	}

	private void cssHandler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(exchange, "/style.css");
		new Thread(clientThread).start();
	}

	private void createHandler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(exchange, "/create");
		new Thread(clientThread).start();
	}

	private void deleteHandler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(exchange, "/delete");
		new Thread(clientThread).start();
	}

	private void writeHandler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(exchange, "/write");
		new Thread(clientThread).start();
	}

	private void readHandler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(exchange, "/read");
		new Thread(clientThread).start();
	}
}
