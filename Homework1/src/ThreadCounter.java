import java.io.FileOutputStream;
import java.util.concurrent.TimeUnit;

public class ThreadCounter implements Runnable
{
	private FileOutputStream log;

	ThreadCounter(FileOutputStream log)
	{
		this.log = log;
	}

	public void run()
	{
		while(true)
		{
			try
			{
				TimeUnit.SECONDS.sleep(1);

				log.write("\n\n----------------------------------------\n".getBytes());
				log.write(("Current active threads: " + Main.activeThreads).getBytes());
				log.write("\n\n----------------------------------------\n".getBytes());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
