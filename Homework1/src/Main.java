import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.sun.security.ntlm.Client;
import org.json.*;

public class Main
{
	private FileOutputStream log;
	public static int activeThreads = 0;
	public static int totalRequests = 0;

	public static int API1 = 0;
	public static int API2 = 0;
	public static int API3 = 0;

	public static void main(String[] argv)
	{
		Main main = new Main();
		main.init();
	}

	public void init()
	{
		try
		{
			log = new FileOutputStream("log.txt");

			ThreadCounter threadCounter = new ThreadCounter(log);
			new Thread(threadCounter).start();

			HttpServer httpServer = HttpServer.create(new InetSocketAddress(3000), 0);
			HttpContext context = httpServer.createContext("/html");
			context.setHandler(this::handler);

			HttpContext context2 = httpServer.createContext("/style");
			context2.setHandler(this::handler2);

			HttpContext context3 = httpServer.createContext("/API1");
			context3.setHandler(this::API1_handler);

			HttpContext context4 = httpServer.createContext("/API2");
			context4.setHandler(this::API2_handler);

			HttpContext context5 = httpServer.createContext("/API3");
			context5.setHandler(this::API3_handler);

			HttpContext context6 = httpServer.createContext("/chart");
			context6.setHandler(this::chart_handler);

			HttpContext context7 = httpServer.createContext("/Chart");
			context7.setHandler(this::chartJS_handler);

			//httpServer.setExecutor(Executors.newCachedThreadPool());
			httpServer.setExecutor(null);
			httpServer.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public void handler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 0, exchange);
		new Thread(clientThread).start();
	}

	public void handler2(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 1, exchange);
		new Thread(clientThread).start();
	}

	public void API1_handler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 2, exchange);
		new Thread(clientThread).start();
	}

	public void API2_handler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 3, exchange);
		new Thread(clientThread).start();
	}

	public void API3_handler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 4, exchange);
		new Thread(clientThread).start();
	}

	public void chart_handler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 5, exchange);
		new Thread(clientThread).start();
	}

	public void chartJS_handler(HttpExchange exchange)
	{
		ClientThread clientThread = new ClientThread(log, 6, exchange);
		new Thread(clientThread).start();
	}
}
