import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class API1
{

	public String dictionaryEntries(String word)
	{
		final String language = "en";
		final String word_id = word.toLowerCase(); //word id is case sensitive and lowercase is required
		return "https://od-api.oxforddictionaries.com:443/api/v1/entries/" + language + "/" + word_id;
	}

	public String getString(String word)
	{
		final String app_id = "bcdb52d3";
		final String app_key = "ec9186c8eb7de5829aa48b87e15c2060";
		try
		{
			URL url = new URL(dictionaryEntries(word));
			HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
			urlConnection.setRequestProperty("Accept","application/json");
			urlConnection.setRequestProperty("app_id", app_id);
			urlConnection.setRequestProperty("app_key", app_key);

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			StringBuilder stringBuilder = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null)
			{
				stringBuilder.append(line + "\n");
			}

			return stringBuilder.toString();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return e.toString();
		}
	}
}
