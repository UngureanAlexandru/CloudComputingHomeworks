import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import com.sun.security.ntlm.Client;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ClientThread implements Runnable
{
	private boolean sendHttpResponse = true;
	private FileOutputStream log;
	private int handlerID;
	private HttpExchange exchange;

	ClientThread(FileOutputStream log, int handlerID, HttpExchange exchange)
	{
		this.log = log;
		this.handlerID = handlerID;
		this.exchange = exchange;
	}

	public void run()
	{
		Main.activeThreads++;
		Main.totalRequests++;

		if (handlerID == 0)
		{
			handler(exchange);
		}
		else if (handlerID == 1)
		{
			handler2(exchange);
		}
		else if (handlerID == 2)
		{
			API1_handler(exchange);
		}
		else if (handlerID == 3)
		{
			API2_handler(exchange);
		}
		else if (handlerID == 4)
		{
			API3_handler(exchange);
		}
		else if (handlerID == 5)
		{
			chart_handler(exchange);
		}
		else if (handlerID == 6)
		{
			chartJS_handler(exchange);
		}

		Main.activeThreads--;
	}

	public void handler(HttpExchange exchange)
	{
		try
		{
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			log.write((timeStamp + " - request: localhost:3000/html\n").getBytes());
			String outputFilePath = "test.html";
			InputStream inputStream = new FileInputStream(outputFilePath);
			byte[] aux = new byte[1];

			int fileSize = 0;
			while (inputStream.read(aux) != -1)
			{
				fileSize++;
			}

			inputStream.close();

			inputStream = new FileInputStream(outputFilePath);

			byte[] response = new byte[fileSize];

			System.out.println("File size = " + fileSize);
			inputStream.read(response, 0, fileSize);

			int responseCode = 200;

			exchange.getResponseHeaders().add("Content-Type", "text/html");
			//exchange.getResponseHeaders().add("Content-Type", "text/css");
			exchange.sendResponseHeaders(responseCode, response.length);

			OutputStream os = exchange.getResponseBody();

			inputStream.close();
			os.write(response);
			os.close();

			log.write("\nResponse:\n".getBytes());
			log.write("========================================================\n\n".getBytes());
			log.write(response);
			log.write("\n\n========================================================\n\n".getBytes());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void handler2(HttpExchange exchange)
	{
		try
		{
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			log.write((timeStamp + " - request: localhost:3000/style\n").getBytes());

			String outputFilePath = "style.css";
			InputStream inputStream = new FileInputStream(outputFilePath);
			byte[] aux = new byte[1];

			int fileSize = 0;
			while (inputStream.read(aux) != -1)
			{
				fileSize++;
			}

			inputStream.close();

			inputStream = new FileInputStream(outputFilePath);

			byte[] response = new byte[fileSize];

			System.out.println("File size = " + fileSize);
			inputStream.read(response, 0, fileSize);

			//System.out.println(new String(response));


			int responseCode = 200;

			//exchange.getResponseHeaders().add("Content-Type", "text/html");
			exchange.getResponseHeaders().add("Content-Type", "text/css");
			exchange.sendResponseHeaders(responseCode, response.length);

			OutputStream os = exchange.getResponseBody();

			//os.write("HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8\n\n".getBytes());
			os.write(response);
			os.close();


			log.write("\nResponse:\n".getBytes());
			log.write("========================================================\n\n".getBytes());
			log.write(response);
			log.write("\n\n========================================================\n\n".getBytes());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}


	public String API1_handler(HttpExchange exchange)
	{
		String valueToReturn = "";
		try
		{
			if (sendHttpResponse)
			{
				String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				log.write((timeStamp + " - request: localhost:3000/API1\n").getBytes());
				Main.API1++;
			}

			API1 api1 = new API1();

			String result = api1.getString("Potato");
			StringBuilder serverResponse = new StringBuilder();

			JSONObject obj = new JSONObject(result);
			JSONArray array = obj.getJSONArray("results");

			for (int i = 0; i < array.length(); i++)
			{
				JSONArray array2 = array.getJSONObject(i).getJSONArray("lexicalEntries");

				for (int j = 0; j < array2.length(); j++)
				{
					JSONArray array3 = array2.getJSONObject(j).getJSONArray("entries");

					for (int k = 0; k < array3.length(); k++)
					{
						JSONArray array4 = array3.getJSONObject(k).getJSONArray("senses");

						for (int l = 0; l < array4.length(); l++)
						{
							JSONArray array5 = array4.getJSONObject(l).getJSONArray("definitions");
							serverResponse.append(array5.toString());
							serverResponse.append('\n');

							System.out.println(array5);
						}
					}
				}
			}
			valueToReturn = serverResponse.toString();

			byte[] response = valueToReturn.getBytes();

			int responseCode = 200;

			if (sendHttpResponse)
			{
				//exchange.getResponseHeaders().add("Content-Type", "text/html");
				//exchange.getResponseHeaders().add("Content-Type", "text/css");
				exchange.sendResponseHeaders(responseCode, response.length);

				OutputStream os = exchange.getResponseBody();
				os.write(response);
				os.close();


				log.write("\nResponse:\n".getBytes());
				log.write("========================================================\n\n".getBytes());
				log.write(response);
				log.write("\n\n========================================================\n\n".getBytes());
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return valueToReturn;
	}

	public String API2_handler(HttpExchange exchange)
	{
		String returnValue = "";
		try
		{
			if (sendHttpResponse)
			{
				String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
				log.write((timeStamp + " - request: localhost:3000/API2\n").getBytes());
				Main.API2++;
			}

			API2 api2 = new API2();
			final String key = "be9615820b9a6cf7c7678abc06423d011694ea079f99e21515be276c58eac981";
			final String topscorers = "https://allsportsapi.com/api/football/?&met=Topscorers&leagueId=258&APIkey=";

			URL url = new URL(topscorers + key);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			InputStream responseStream = con.getInputStream();

			StringBuilder jsonString = new StringBuilder();

			int streamByte = 0;
			while((streamByte = responseStream.read()) != -1)
			{
				jsonString.append((char)streamByte);
			}

			responseStream.close();
			con.disconnect();

			String parsedData = api2.parseData(jsonString.toString());

			returnValue = parsedData.toString();
			System.out.println(returnValue);

			byte[] response = parsedData.toString().getBytes();

			int responseCode = 200;

			if (sendHttpResponse)
			{
				//exchange.getResponseHeaders().add("Content-Type", "text/html");
				//exchange.getResponseHeaders().add("Content-Type", "text/css");
				exchange.sendResponseHeaders(responseCode, response.length);

				OutputStream os = exchange.getResponseBody();
				os.write(response);
				os.close();

				log.write("\nResponse:\n".getBytes());
				log.write("========================================================\n\n".getBytes());
				log.write(response);
				log.write("\n\n========================================================\n\n".getBytes());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return returnValue;
	}

	public void API3_handler(HttpExchange exchange)
	{
		try
		{
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			log.write((timeStamp + " - request: localhost:3000/API3\n").getBytes());
			Main.API3++;

			sendHttpResponse = false;

			String api1Result = API1_handler(exchange);
			String api2Result = API2_handler(exchange);

			API3 api3 = new API3();

			String result2 = "";
			final String DEV_KEY = "a1026bfb6aa2a42222b10ecbdde3a670";

			StringBuilder toBePasted = new StringBuilder();
			toBePasted.append("API1 result:\n");
			toBePasted.append(api1Result);
			toBePasted.append('\n');
			toBePasted.append("==========================================");
			toBePasted.append('\n');
			toBePasted.append("API2 result:\n");
			toBePasted.append(api2Result);


			String api_paste_code = toBePasted.toString();
			String api_paste_private = "1";
			String api_paste_name = "Test paste";
			String api_paste_expire_date = "10M";
			String api_paste_format = "text";
			String api_user_key = "";

			//api_paste_name = URLEncoder.encode(api_paste_name, "UTF-8");
			//api_paste_code = URLEncoder.encode(api_paste_code, "UTF-8");

			String api_url = "https://pastebin.com/api/api_post.php";
			//$ch = curl_init($url);

			URL url = new URL(api_url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");

			Map<String, String> parameters = new HashMap<>();
			parameters.put("api_option", "paste");
			parameters.put("api_user_key", api_user_key);
			parameters.put("api_paste_private", api_paste_private);
			parameters.put("api_paste_name", api_paste_name);
			parameters.put("api_paste_expire_date", api_paste_expire_date);
			parameters.put("api_paste_format", api_paste_format);
			parameters.put("api_dev_key", DEV_KEY);
			parameters.put("api_paste_code", api_paste_code);

//api_option=paste & api_user_key='.$api_user_key.'&api_paste_private='.$api_paste_private.
// '&api_paste_name='.$api_paste_name.'&api_paste_expire_date='.$api_paste_expire_date.
// '&api_paste_format='.$api_paste_format.'&api_dev_key='.$api_dev_key.
// '&api_paste_code='.$api_paste_code.'');
			con.setDoOutput(true);

			DataOutputStream out = new DataOutputStream(con.getOutputStream());
			out.writeBytes(api3.getParamsString(parameters));
			out.flush();
			out.close();

			InputStream responseStream = con.getInputStream();

			StringBuilder jsonString = new StringBuilder();

			int streamByte = 0;
			while((streamByte = responseStream.read()) != -1)
			{
				jsonString.append((char)streamByte);
			}

			responseStream.close();
			con.disconnect();

			System.out.println(jsonString.toString());
			result2 = jsonString.toString();

			byte[] response = result2.getBytes();

			int responseCode = 200;

			//exchange.getResponseHeaders().add("Content-Type", "text/html");
			//exchange.getResponseHeaders().add("Content-Type", "text/css");
			exchange.sendResponseHeaders(responseCode, response.length);

			OutputStream os = exchange.getResponseBody();

			os.write(response);
			os.close();

			log.write("\nResponse:\n".getBytes());
			log.write("========================================================\n\n".getBytes());
			log.write(response);
			log.write("\n\n========================================================\n\n".getBytes());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void chart_handler(HttpExchange exchange)
	{
		try
		{
			byte[] response = ("<!DOCTYPE html>\n" + "<html>\n" + "<head>\n"
					+ "    <title></title>\n" + "</head>\n" + "<body>\n" + "\n" + "<canvas id=\"myChart\" width=\"400\" height=\"100\"></canvas>\n"
					+ "<script src = \"Chart.js\"></script>\n" + "<script>\n" + "var ctx = document.getElementById(\"myChart\");\n" + "\n"
					+ "var myLineChart = new Chart(ctx, {\n" + "    type: 'bar',\n" + "    data:\n" + "    {\n"
					+ "        labels : ['Api 1', 'Api 2', 'Api 3'],\n" + "        datasets: \n" + "        [{\n"
					+ "            label : 'Users preferences',\n" + "            data : \n"
					+ "            [{\n"
					+ "                x : 10,\n"
					+ "                y : " + Main.API1 + "\n"
					+ "            },\n" + "            {\n"
					+ "                x : 40,\n"
					+ "                y : " + Main.API2 + "\n"
					+ "            },\n" + "            {\n"
					+ "                x : 15,\n"
					+ "                y : " + Main.API3 + "\n"
					+ "            }],\n"
					+ "            backgroundColor :\n" + "            [\n" + "                'rgba(0, 0, 255, 0.2)',\n"
					+ "                'rgba(0, 0, 255, 0.2)',\n" + "                'rgba(0, 0, 255, 0.2)'\n" + "            ],\n"
					+ "            borderColor :\n" + "            [\n" + "                'rgba(0, 0, 255, 0.2)',\n"
					+ "                'rgba(0, 0, 255, 0.2)',\n" + "                'rgba(0, 0, 255, 0.2)'\n" + "            ],\n"
					+ "            fill : false,\n" + "            borderColor : \"#3cba9f\",\n"
					+ "            borderWidth : 1\n" + "        }]\n" + "    },\n" + "    options: {\n" + "        scales: {\n"
					+ "            yAxes: [{\n" + "                ticks: {\n" + "                    beginAtZero:true\n" + "                }\n"
					+ "            }]\n" + "        }\n" + "    }\n" + "});\n" + "\n" + "</script>\n" + "\n" + "</body>\n" + "</html>").getBytes();

			int responseCode = 200;

			//exchange.getResponseHeaders().add("Content-Type", "application/javascript");
			exchange.getResponseHeaders().add("Content-Type", "text/html");

			exchange.sendResponseHeaders(responseCode, response.length);
			OutputStream os = exchange.getResponseBody();

			os.write(response);
			os.close();

//			log.write("\nResponse:\n".getBytes());
//			log.write("========================================================\n\n".getBytes());
//			log.write(response);
//			log.write("\n\n========================================================\n\n".getBytes());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void chartJS_handler(HttpExchange exchange)
	{
		try
		{
			InputStream chartFile = new FileInputStream("Chart.js");

			byte[] response = new byte[402654];
			chartFile.read(response, 0, 402654);

			int responseCode = 200;
			exchange.getResponseHeaders().add("Content-Type", "application/javascript");

			exchange.sendResponseHeaders(responseCode, response.length);
			OutputStream os = exchange.getResponseBody();

			os.write(response);
			os.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
