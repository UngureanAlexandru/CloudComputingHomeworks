import org.json.JSONArray;
import org.json.JSONObject;

public class API2
{
	public String parseData(String data)
	{
		JSONObject root = new JSONObject(data);
		JSONArray array = root.getJSONArray("result");

		StringBuilder stringResponse = new StringBuilder();

		for (int i = 0; i < 10; i++)
		{
			JSONObject playerData = array.getJSONObject(i);
			//String name = playerData.getJSONObject(1).toString();
			//System.out.println(name);
			stringResponse.append("Place: ");
			stringResponse.append(playerData.getString("player_place"));
			stringResponse.append(", Name: ");
			stringResponse.append(playerData.getString("player_name"));
			stringResponse.append(", Team: ");
			stringResponse.append(playerData.getString("team_name"));
			stringResponse.append(", Goals: ");
			stringResponse.append(playerData.getString("goals"));
			stringResponse.append('\n');
		}
		return stringResponse.toString();
	}
}
